import React from "react";
import ReactDOM from "react-dom";

import { Messenger } from "./components/Messenger";

import "./style.css";

const App = () => {
  return <Messenger />;
};

ReactDOM.render(<App />, document.querySelector("#root"));
