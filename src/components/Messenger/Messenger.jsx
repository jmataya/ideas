import React, { useState } from "react";
import { Container } from "../Container";
import { TextInput } from "../Forms";
import { Header } from "../Header";
import { Text, Title } from "../Typography";
import "./messenger.css";

export function Messenger() {
  const [alias, setAlias] = useState("");
  const onChange = ({ target }) => setAlias(target.value);

  return (
    <div className="messenger">
      <Header>
        Ideas Messenger
      </Header>
      <div className="messenger__body">
        <Container>
          <Title>Let's get started!</Title>
          <Text>
            Create an alias and then connect to the network.
          </Text>
          <form className="messenger__form">
            <TextInput id="alias"
                       label="Alias"
                       value={alias}
                       onChange={onChange}
            />
          </form>
        </Container>
      </div>
    </div>
  );
}
