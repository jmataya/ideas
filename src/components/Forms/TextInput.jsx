import React from "react";
import "./textInput.css";

export function TextInput({id, label, value, onChange}) {
  return (
    <div className="textInput">
      <input type="text" id={id} value={value} onChange={onChange} /> 
      <label for={id}>{label}</label>
    </div>
  );
}