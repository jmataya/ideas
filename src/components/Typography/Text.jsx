import React from "react";
import "./text.css";

export function Text({children}) {
  return <p className="text">{children}</p>;
}