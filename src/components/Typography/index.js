import React from "react";
import { Text } from "./Text.jsx";
import { Title } from "./Title.jsx";

export { Text, Title };
